/*
 * Copyright (c) 2014, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package org.openjdk.jmh.results;

import org.openjdk.jmh.runner.options.TimeValue;
import org.openjdk.jmh.util.ListStatistics;
import org.openjdk.jmh.util.Statistics;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class CustomResult extends Result<CustomResult> {

    public CustomResult(ResultRole role, String label, double score, TimeUnit outputTimeUnit) {
        this(role, label, of(score), TimeValue.tuToString(outputTimeUnit) + "/op");
    }

    CustomResult(ResultRole mode, String label, Statistics s, String unit) {
        super(mode, label, s, unit, AggregationPolicy.AVG);
    }

    @Override
    protected Aggregator<CustomResult> getThreadAggregator() {
        return new CustomResult.AveragingAggregator();
    }

    @Override
    protected Aggregator<CustomResult> getIterationAggregator() {
        return new CustomResult.AveragingAggregator();
    }

    static class AveragingAggregator implements Aggregator<CustomResult> {
        @Override
        public CustomResult aggregate(Collection<CustomResult> results) {
            ListStatistics stat = new ListStatistics();
            for (CustomResult r : results) {
                stat.addValue(r.getScore());
            }
            return new CustomResult(
                    AggregatorUtils.aggregateRoles(results),
                    AggregatorUtils.aggregateLabels(results),
                    stat,
                    AggregatorUtils.aggregateUnits(results)
            );
        }

    }

}
